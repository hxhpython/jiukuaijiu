from django.core.paginator import Paginator
#多对象返回 ----分页
class MultiObjectReturn():
    #每一页的商品数量
    per_page = 12
    #获取当前类别 所有商品对象列表集合
    objects = None
    objects_name = 'objects'
    def get_objects(self,page_num):
        #获得分页对象
        paginator = Paginator(self.objects,self.per_page)
        #当前页码
        page_num = int(page_num)
        #确保页码在正确范围内
        if page_num < 1:
            page_num = 1
        if page_num > paginator.num_pages:
            page_num = paginator.num_pages
        #获得当前 页Page对象
        page = paginator.page(page_num)
        return {'page':page,'page_range':paginator.page_range,self.objects_name:page.object_list}
