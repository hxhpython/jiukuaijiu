from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
#基本视图
class BaseView(View):
    template_name = None
    #重写HTTP get 请求
    def get(self,request,*args,**kwargs):
        if hasattr(self,'prepare'):
            getattr(self,'prepare')(request,*args,**kwargs)
        #返回渲染数据
        response = render(request,self.template_name,self.get_context(request))

        return response
    def get_context(self,request):
        context = {}
        context.update(self.get_extra_context(request))
        # print('get_conetxt',context['categorys'])
        return context

    def get_extra_context(self,request):
        pass
#重定向
class BaseRedirectView(View):
    redirect_url = None
    def dispatch(self, request, *args, **kwargs):
        if hasattr(self,'handler'):
            getattr(self,'handler')(request,*args,**kwargs)
        return HttpResponseRedirect(self.redirect_url)
