from django.db import models

# Create your models here.
#类别表
class Category(models.Model):
    name = models.CharField(unique=True,max_length=255,verbose_name='类别名称')
    def __str__(self):
        return u'%s'%self.name
    class Meta(object):
        verbose_name_plural ='类别'
        ordering = ['id']

#商品表
class Goods(models.Model):
    name = models.CharField(max_length=255,verbose_name='商品名称')
    desc = models.CharField(max_length=255,verbose_name='商品描述')
    price = models.DecimalField(max_digits=5,decimal_places=2,verbose_name='商品价格')
    oldprice = models.DecimalField(max_digits=5,decimal_places=2,verbose_name='商品原价')
    category = models.ForeignKey(Category,verbose_name='类别')
    def __str__(self):
        return u'%s'%self.name
    class Meta(object):
        verbose_name_plural ='商品'
        ordering = ['id']
    def img(self):
        return self.sku_set.first().color.value
    def colors(self):
        colors =[]
        for sku in self.sku_set.all():
            color = sku.color
            if color not in colors:
                colors.append(color)
        return colors
    def sizes(self):
        sizes =[]
        for sku in self.sku_set.all():
            size = sku.size
            if size not in sizes:
                sizes.append(size)
        return sizes
    def details(self):
        import collections
        #有序字典
        datas = collections.OrderedDict()
        for details in self.goodsdetailsitem_set.all():
            if details.name() not in datas:
                datas[details.name()] =[details.value]
                print(details.name())
            else:
                datas[details.name()].append(details.value)
                print(details.name())
        return datas
#样式表
class Size(models.Model):
    name =models.CharField(max_length=20,verbose_name='名称')
    value = models.CharField(max_length=20,verbose_name='值')
    def __str__(self):
        return u'%s' % self.name
    class Meta(object):
        verbose_name_plural = '尺寸'
        ordering = ['id']

#颜色表
class Color(models.Model):
    #colorname 不唯一
    name = models.CharField(max_length=20,verbose_name='名称')
    #upload设置的内容media_root 文件中的子目录用来存放上传文件
    value = models.ImageField(upload_to='media/color/',verbose_name='值')
    def __str__(self):
        return u'%s' % self.name
    class Meta(object):
        verbose_name_plural = '颜色'
        ordering = ['id']

class Goodsdetails(models.Model):
    # goods = models.ForeignKey(Goods)
    name = models.CharField(max_length=20,unique=True,verbose_name='名称')
    def __str__(self):
        return u'%s' % self.name
    class Meta(object):
        verbose_name_plural = '详情'
        ordering = ['id']
#详情页的每一张图片

class GoodsdetailsItem(models.Model):
    goods = models.ForeignKey(Goods)
    goodsdetails = models.ForeignKey(Goodsdetails,verbose_name='商品详情')
    value = models.ImageField(upload_to='media/',verbose_name='图片')
    def __str__(self):
        return u'%s' %self.goodsdetails.name
    class Meta(object):
        verbose_name_plural = '详情条目'
        ordering = ['id']
    def name(self):
        return self.goodsdetails.name
#仓库
class SKU(models.Model):
    color =models.ForeignKey(Color,verbose_name='颜色')
    size  =models.ForeignKey(Size,verbose_name='尺寸')
    goods =models.ForeignKey(Goods,verbose_name='商品')
    count =models.IntegerField(default=100,verbose_name='库存')
    class Meta(object):
        verbose_name_plural = '库存'
        ordering = ['id']










