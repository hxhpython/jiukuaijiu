from django.shortcuts import render
from view.myview import *
from goods.models import *
from utils.pageutils import MultiObjectReturn
# Create your views here.
class GoodsListView(BaseView,MultiObjectReturn):
    template_name = 'index.html'
    #需要获取的商品
    objects_name = 'goods'
    #获得所有的类别对象
    category_objects= Category.objects.all()
    # 准备方法
    def prepare(self,request,categoryid=None):
        #获得当前类
        if categoryid == None:categoryid=1
        self.category_id = categoryid
        #获取当前类别 所有商品
        self.objects = Category.objects.get(id =self.category_id).goods_set.all()
    def get_extra_context(self,request):
        #获取当前页码 默认1
        page_num = request.GET.get('page',1)
        context = {'category_id':self.category_id,'categorys':self.category_objects}
        context.update(self.get_objects(page_num))
        return context

class GoodsdetailsView(BaseView):
    template_name = 'details.html'
    def get_extra_context(self,request,goodid=None):
        if goodid == None: goodid = 1
        self.goodid = goodid
        good = Goods.objects.get(id =goodid)
        # imgs = good.goodsdetailsitem_set.name.values()

        context = {'good':good}
        return context