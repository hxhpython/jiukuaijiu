from django.contrib import admin
from goods.models import *
# Register your models here.

class GoodsDetailsItemInline(admin.TabularInline):
    model =GoodsdetailsItem
    extra = 1
class SKUInline(admin.TabularInline):
    model = SKU
    extra = 1
class GoodsDetailsItemModelAdmin(admin.ModelAdmin):
    inlines = [GoodsDetailsItemInline]
class GoodsModelAdmin(admin.ModelAdmin):
    list_display = ['name','desc','price','oldprice']
    list_per_page =10
    search_fields = ['name']
    list_filter = ['category__name']
    inlines = [GoodsDetailsItemInline,SKUInline]
class SKUModelAdmin(admin.ModelAdmin):
    list_display = ['goods','color','size','count']
    list_per_page = 10
    list_filter = ['goods']
    search_fields = ['goods__name']
# class GoodsDetailsItemModelAdmin(admin.ModelAdmin):
#     inlines = [GoodsDetailsItemInline]

admin.site.register(Category)
admin.site.register(Goods,GoodsModelAdmin)
admin.site.register(Color)
admin.site.register(Size)
admin.site.register(Goodsdetails)
admin.site.register(GoodsdetailsItem)
admin.site.register(SKU,SKUModelAdmin)