from django.conf.urls import url
from goods import views
urlpatterns = [
    url(r'^$',views.GoodsListView.as_view()),
    url(r'^category/(\d+)$',views.GoodsListView.as_view()),
    url(r'^.*/goods/(\d+)$',views.GoodsdetailsView.as_view()),
]